﻿/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections;
using Android.Content;
using Android.Views;
using Android.Widget;
using QuasarTech.Xamarin.AndroidSupport.Utils;

namespace QuasarTech.Xamarin.AndroidSupport.UI.Adapters
{
	public class EnhancedSpinnerAdapter : ArrayAdapter
	{
		public EnhancedSpinnerAdapter(Context context, int resource, IList objects)
			: base(context, resource, objects)
		{
	    }

		public EnhancedSpinnerAdapter(Context context, int layoutId, int textViewId, IList objects)
			: base(context, layoutId, textViewId, objects)
		{
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View v = base.GetView(position, convertView, parent);

			v.FindAndInitializeTextViewById(Android.Resource.Id.Text1);            

			return v;
		}

		public override View GetDropDownView(int position, View convertView, ViewGroup parent)
		{
			View v = base.GetDropDownView(position, null, parent);
			v.FindAndInitializeTextViewById(Android.Resource.Id.Text1);            

			return v;
		}

		public new static EnhancedSpinnerAdapter CreateFromResource(Context context, int dataResource, int layoutResource)
		{
			return new EnhancedSpinnerAdapter(context, layoutResource, context.Resources.GetStringArray(dataResource));
		}
	}
}

