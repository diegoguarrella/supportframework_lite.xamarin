﻿/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using QuasarTech.Xamarin.AndroidSupport.Commons;

namespace QuasarTech.Xamarin.AndroidSupport.Utils
{
    public static class CommonUtils
    {
        public static DateTime dateJan1st1970 = new DateTime(1970, 1, 1);

        public static void ClearSafely(this ArrayList obj)
        {
            if (obj != null)
                obj.Clear();
        }

        public static void ClearSafely<T>(this ICollection<T> obj)
        {
            if (obj != null)
                obj.Clear();
        }

        public static void DisposeSafely(this IDisposable obj)
        {
            if (obj != null)
                obj.Dispose();
        }
        
        public static long GetTimestamp()
        {
            return (long)(DateTime.UtcNow - dateJan1st1970).TotalMilliseconds;
        }

        public static Int32 GetTime()
        {
            //JS GetTime() porting
            return (Int32)(((DateTime.Now.ToUniversalTime() - dateJan1st1970)).TotalMilliseconds + 0.5);
        }

        public static int IndexOf<TKey, TValue>(this Dictionary<TKey, TValue> dict, TValue obj)
        {
            int index = 0;

            foreach (TKey v in dict.Keys)
            {
                if (v.Equals(obj))
                    return index;
                index++;
            }

            index = 0;
            foreach (TValue v in dict.Values)
            {
                if (v.Equals(obj))
                    return index;
                index++;
            }
            return -1;
        }
        

        public static TextView FindAndInitializeTextViewById(this Activity container, int resId, string text = null, bool localizedResource = true)
        {
            TextView v = container.FindViewById<TextView>(resId);
            if (v != null)
            {
                if (text != null)
                    v.Text = localizedResource ? null : text;
            }
            return v;
        }

        public static TextView FindAndInitializeTextViewById(this View container, int resId, string text = null, bool localizedResource = true)
        {
            TextView v = container.FindViewById<TextView>(resId);
            if (v != null)
            {
                if (text != null)
                    v.Text = localizedResource ?  null : text;
            }
            return v;
        }

        public static void HideSoftKeyBoard(this View v)
        {
            InputMethodManager imm = (InputMethodManager)v.Context.GetSystemService(Context.InputMethodService);

            if (v.WindowToken != null)
                imm.HideSoftInputFromWindow(v.WindowToken, 0);
        }

        public static T Instantiate<T> (this Type objType) where T : class {
            var propertyInfo = (objType as Type).GetProperty("Instance", BindingFlags.Public | BindingFlags.Static);
            return propertyInfo == null ? null : propertyInfo.GetValue(objType, null) as T;
        }

		public static JavaObjectWrapper<T> JavaWrap<T> (this T obj) where T: class {
			return new JavaObjectWrapper<T> { WrappedObject = obj };
		}

		public static T JavaUnwrap<T>(this Object obj) where T : class {
			var pObj = obj.GetType ().GetProperty ("WrappedObject");
			return pObj == null ? null : pObj.GetValue (obj) as T;
		}

        public static void SetHeightBasedOnChildren(this ListView listView)
        {
            if (listView.Adapter == null)
            {
                // pre-condition
                return;
            }

            int totalHeight = listView.PaddingTop + listView.PaddingBottom;
            for (int i = 0; i < listView.Count; i++)
            {
                View listItem = listView.Adapter.GetView(i, null, listView);
                if (listItem.GetType() == typeof(ViewGroup))
                    listItem.LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);

                listItem.Measure(0, 0);
                totalHeight += listItem.MeasuredHeight;
            }

            listView.LayoutParameters.Height = totalHeight + (listView.DividerHeight * (listView.Count - 1));
        }
    }

    public interface IInstantiableSingletonObject
    {
        IInstantiableSingletonObject Instance { get; }
    }

    public class JavaObjectWrapper<T> : Java.Lang.Object
    {
        public T WrappedObject { get; set; }
    }

}