/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Xamarin.NineOldAndroids.Views;

namespace QuasarTech.Xamarin.AndroidSupport.UI
{
    public class ParallaxPageTransformer : Object, ViewPager.IPageTransformer
    {
        private readonly int viewId;

        public ParallaxPageTransformer(int viewId)
        {
            this.viewId = viewId;
        }

        public void TransformPage(View view, float position)
        {
            ImageView ivBackground = view.FindViewById<ImageView>(viewId);
            int pageWidth = view.Width;

            if (position < -1)
            {
                // [-Infinity,-1)
                // This page is way off-screen to the left.
                ViewHelper.SetAlpha(view, 1.0f);
            }
            else if (position <= 1)
            {
                // [-1,1]
                ViewHelper.SetTranslationX(ivBackground, -position * (pageWidth / 2)); //Half the normal speed
            }
            else
            {
                // (1,+Infinity]
                // This page is way off-screen to the right.
                ViewHelper.SetAlpha(view, 1.0f);
            }
        }
    }
}