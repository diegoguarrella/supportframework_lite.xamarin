﻿/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace QuasarTech.Xamarin.AndroidSupport.UI.Widgets
{
    public class EnhancedScrollView : ScrollView
    {
        public View HeaderView;
        public IEnhancedScrollViewListener ScrollListener = null;
        private int lastTopValueAssigned = 0;

        public EnhancedScrollView(Context context)
            : base(context)
        {
        }

        public EnhancedScrollView(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle)
        {
        }

        public EnhancedScrollView(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
        }

        protected override void OnScrollChanged(int l, int t, int oldl, int oldt)
        {
            base.OnScrollChanged(l, t, oldl, oldt);

            if (ScrollListener != null)
            {
                ScrollListener.OnScrollChanged(this, l, t, oldl, oldt);
            }

            if (HeaderView != null && Build.VERSION.SdkInt >= BuildVersionCodes.Gingerbread)
                ParallaxImage(HeaderView);
        }

        private void ParallaxImage(View view)
        {
            Rect rect = new Rect();
            view.GetLocalVisibleRect(rect);
            if (lastTopValueAssigned != rect.Top)
            {
                lastTopValueAssigned = rect.Top;
                view.SetY((float)(rect.Top / 2.0));
            }
        }
    }

    public interface IEnhancedScrollViewListener
    {
        void OnScrollChanged(EnhancedScrollView scrollView, int x, int y,
            int oldx, int oldy);
    }
}