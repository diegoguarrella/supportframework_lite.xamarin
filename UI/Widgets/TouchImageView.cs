/* 
 * Xamarin porting from: https://stackoverflow.com/questions/22324793/touch-image-zooming-in-android
 *
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using Android.Content;
using Android.Graphics;
using Android.Util;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace QuasarTech.Xamarin.AndroidSupport.UI.Widgets
{
    public class TouchImageView : ImageView
    {
        private Matrix matrix;

        // We can be in one of these 3 states
        private const int NONE = 0;

        private const int DRAG = 1;
        private const int ZOOM = 2;
        private int mode = NONE;

        // Remember some things for zooming
        private PointF last = new PointF();

        private PointF start = new PointF();
        private float minScale = 1f;
        private float maxScale = 3f;
        private float[] m;

        private int viewWidth, viewHeight;
        private const int CLICK = 3;
        private float saveScale = 1f;
        protected float origWidth, origHeight;
        private int oldMeasuredWidth, oldMeasuredHeight;

        private ScaleGestureDetector mScaleDetector;
        private Context context;

        public TouchImageView(Context context)
            : base(context)
        {
            SharedConstructing(context);
        }

        public TouchImageView(Context context, IAttributeSet attrs)
            : base(context, attrs)
        {
            SharedConstructing(context);
        }

        private void SharedConstructing(Context context)
        {
            base.Clickable = true;

            this.context = context;
            mScaleDetector = new ScaleGestureDetector(context, new ScaleListener(this));
            matrix = new Matrix();
            m = new float[9];

            this.ImageMatrix = matrix;

            SetScaleType(ScaleType.Matrix);
            this.Touch +=

                (object o, TouchEventArgs mEvent) =>
                {
                    mScaleDetector.OnTouchEvent(mEvent.Event);
                    PointF curr = new PointF(mEvent.Event.GetX(), mEvent.Event.GetY());

                    switch (mEvent.Event.Action)
                    {
                        case MotionEventActions.Down:
                            last.Set(curr);
                            start.Set(last);
                            mode = DRAG;
                            break;

                        case MotionEventActions.Move:
                            if (mode == DRAG)
                            {
                                float deltaX = curr.X - last.X;
                                float deltaY = curr.Y - last.Y;
                                float FixTransX = GetFixDragTrans(deltaX, viewWidth,
                                        origWidth * saveScale);
                                float FixTransY = GetFixDragTrans(deltaY, viewHeight,
                                        origHeight * saveScale);
                                matrix.PostTranslate(FixTransX, FixTransY);
                                FixTrans();
                                last.Set(curr.X, curr.Y);
                            }
                            break;

                        case MotionEventActions.Up:
                            mode = NONE;
                            int xDiff = (int)Math.Abs(curr.X - start.X);
                            int yDiff = (int)Math.Abs(curr.Y - start.Y);
                            if (xDiff < CLICK && yDiff < CLICK)
                                PerformClick();
                            break;

                        case MotionEventActions.PointerUp:
                            mode = NONE;
                            break;
                    }

                    ImageMatrix = matrix;
                    Invalidate();
                };
        }

        public void SetMaxZoom(float x)
        {
            maxScale = x;
        }

        internal class ScaleListener : ScaleGestureDetector.SimpleOnScaleGestureListener
        {
            private TouchImageView ivView;

            public ScaleListener(TouchImageView ivView)
            {
                this.ivView = ivView;
            }

            public override bool OnScaleBegin(ScaleGestureDetector detector)
            {
                ivView.mode = ZOOM;
                return true;
            }

            public override bool OnScale(ScaleGestureDetector detector)
            {
                float mScaleFactor = detector.ScaleFactor;
                float origScale = ivView.saveScale;
                ivView.saveScale *= mScaleFactor;
                if (ivView.saveScale > ivView.maxScale)
                {
                    ivView.saveScale = ivView.maxScale;
                    mScaleFactor = ivView.maxScale / origScale;
                }
                else if (ivView.saveScale < ivView.minScale)
                {
                    ivView.saveScale = ivView.minScale;
                    mScaleFactor = ivView.minScale / origScale;
                }

                if (ivView.origWidth * ivView.saveScale <= ivView.viewWidth
                        || ivView.origHeight * ivView.saveScale <= ivView.viewHeight)
                    ivView.matrix.PostScale(mScaleFactor, mScaleFactor, ivView.viewWidth / 2,
                            ivView.viewHeight / 2);
                else
                    ivView.matrix.PostScale(mScaleFactor, mScaleFactor,
                            detector.FocusX, detector.FocusY);

                ivView.FixTrans();
                return true;
            }
        }

        private void FixTrans()
        {
            matrix.GetValues(m);
            float transX = m[Matrix.MtransX];
            float transY = m[Matrix.MtransY];

            float FixTransX = getFixTrans(transX, viewWidth, origWidth * saveScale);
            float FixTransY = getFixTrans(transY, viewHeight, origHeight
                    * saveScale);

            if (FixTransX != 0 || FixTransY != 0)
                matrix.PostTranslate(FixTransX, FixTransY);
        }

        private float getFixTrans(float trans, float viewSize, float contentSize)
        {
            float minTrans, maxTrans;

            if (contentSize <= viewSize)
            {
                minTrans = 0;
                maxTrans = viewSize - contentSize;
            }
            else
            {
                minTrans = viewSize - contentSize;
                maxTrans = 0;
            }

            if (trans < minTrans)
                return -trans + minTrans;
            if (trans > maxTrans)
                return -trans + maxTrans;
            return 0;
        }

        private float GetFixDragTrans(float delta, float viewSize, float contentSize)
        {
            if (contentSize <= viewSize)
            {
                return 0;
            }
            return delta;
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {
            base.OnMeasure(widthMeasureSpec, heightMeasureSpec);
            viewWidth = MeasureSpec.GetSize(widthMeasureSpec);
            viewHeight = MeasureSpec.GetSize(heightMeasureSpec);

            //
            // Rescales image on rotation
            //
            if (oldMeasuredHeight == viewWidth && oldMeasuredHeight == viewHeight
                    || viewWidth == 0 || viewHeight == 0)
                return;
            oldMeasuredHeight = viewHeight;
            oldMeasuredWidth = viewWidth;

            if (saveScale == 1)
            {
                // Fit to screen.

                var drawable = this.Drawable;
                if (drawable == null || drawable.IntrinsicWidth == 0
                        || drawable.IntrinsicHeight == 0)
                    return;
                int bmWidth = drawable.IntrinsicWidth;
                int bmHeight = drawable.IntrinsicHeight;

                Log.Debug("bmSize", "bmWidth: " + bmWidth + " bmHeight : " + bmHeight);

                float scaleX = viewWidth / (float)bmWidth;
                float scaleY = viewHeight / (float)bmHeight;
                var scale = Math.Min(scaleX, scaleY);
                matrix.SetScale(scale, scale);

                // Center the image
                float redundantYSpace = viewHeight
                        - (scale * bmHeight);
                float redundantXSpace = viewWidth
                        - (scale * bmWidth);
                redundantYSpace /= 2;
                redundantXSpace /= 2;

                matrix.PostTranslate(redundantXSpace, redundantYSpace);

                origWidth = viewWidth - 2 * redundantXSpace;
                origHeight = viewHeight - 2 * redundantYSpace;

                this.ImageMatrix = matrix;
            }
            FixTrans();
        }
    }
}