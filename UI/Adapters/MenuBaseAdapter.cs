/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using Android.Content;
using Android.Graphics;
using Android.Views;
using Android.Widget;
using Java.Lang;
using QuasarTech.Xamarin.AndroidSupport.Data;

namespace QuasarTech.Xamarin.AndroidSupport.UI.Adapters
{
    /// <summary>
    /// This class extend MenuBaseAdapter
    /// </summary>
    public class MenuBaseAdapter : EnhancedBaseAdapter
    {
        #region Fields & Variables

        private readonly Color colorHighlight;
        private List<NavigationItem> entryList; // data list

        #endregion Fields & Variables

        #region Generals

        public MenuBaseAdapter(Context context, List<NavigationItem> entryList, int highlightColor)
            : base(context)
        {
            this.context = context;
            this.entryList = entryList;
            this.inflater = LayoutInflater.From(context);

            colorHighlight = context.Resources.GetColor(highlightColor);
        }

        public void ChangeData(List<NavigationItem> list)
        {
            this.entryList = list;
        }

        public NavigationItem GetMenuItem(int position)
        {
            return entryList[position];
        }

        #endregion Generals

        #region Overrides

        public override int Count
        {
            get { return entryList != null ? entryList.Count : 0; }
        }

        public override Object GetItem(int position)
        {
            return entryList[position];
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup viewGroup)
        {
            ViewHolder holder = null;

            // Get Track data
            NavigationItem entry = entryList[position];

            // If convertView isn't null, reuse it. Otherwise, inflate a new view
            if (convertView != null)
                holder = (ViewHolder)convertView.Tag;

            if (convertView == null || (holder.IsHeader != entry.IsHeader))
            {
                convertView = inflater.Inflate(entry.IsHeader ? Resource.Layout.drawer_list_header : Resource.Layout.entry_navigation, viewGroup, false);
                holder = new ViewHolder
                {
                    tvTitle = (TextView)convertView.FindViewById(Android.Resource.Id.Text1),
                    icon = (ImageView)convertView.FindViewById(Android.Resource.Id.Icon1),
                    selectionHandler = convertView.FindViewById(Resource.Id.selectionHandler),
                    llBackground = (LinearLayout)convertView.FindViewById(Resource.Id.llBackground),
                    IsHeader = entry.IsHeader
                };

                // Assign the viewHolder
                convertView.Tag = holder;
            }
            else
                holder = (ViewHolder)convertView.Tag;

            // Attenzione: il parametro di setClickable funziona al contrario: false
            // indica che l'elemento � cliccabile

            holder.tvTitle.SetTextColor(Color.White);
            holder.tvTitle.Text = entry.Title;
                        
            holder.icon.SetAlpha(255);

            if (entry.IsHeader)
            {
                convertView.Clickable = true;
            }
            else
            {
                holder.icon.SetImageResource(entry.Icon);
                holder.icon.SetAlpha(255);
                if (entry.Selected)
                {
                    holder.selectionHandler.Visibility = ViewStates.Visible;
                    holder.llBackground.SetBackgroundColor(colorHighlight);
                }
                else
                {
                    holder.selectionHandler.Visibility = ViewStates.Invisible;
                    holder.llBackground.SetBackgroundColor(Color.Transparent);
                }

                convertView.Clickable = false;
            }

            return convertView;
        }

        #endregion Overrides

        #region ViewHolder

        public class ViewHolder : Object
        {
            public ImageView icon;
            public View selectionHandler;
            public LinearLayout llBackground;
            public TextView tvTitle;
            public bool IsHeader;
        }

        #endregion ViewHolder
    }
}