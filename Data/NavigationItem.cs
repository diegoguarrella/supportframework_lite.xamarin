/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Object = Java.Lang.Object;
using String = System.String;

namespace QuasarTech.Xamarin.AndroidSupport.Data
{
    public class NavigationItem : Object
    {
        #region Fields & Variables

        public List<NavigationItem> Children;
        public int MenuLevel = 0;
        public NavigationItem Parent;
        public bool Selected = false;
        public String Title;
        public bool Visible = false;
        public bool IsHeader = false;
        public int Icon;
        public object Destination;

        #endregion Fields & Variables

        #region .Ctor

        public NavigationItem(int menuLevel)
        {
            this.MenuLevel = menuLevel;
        }

        public NavigationItem(String title, int icon, System.Object destination)
        {
            this.Title = title;
            this.Destination = destination;
        }

        public NavigationItem()
        {
        }

        public NavigationItem(String title, int icon, System.Object destination,
            bool visible, bool off)
        {
            this.Title = title;
            this.Icon = icon;
            this.Destination = destination;
            this.Visible = visible;
        }

        public NavigationItem(String title, int icon, System.Object destination,
            bool visible, bool off, bool isHeader)
        {
            this.Title = title;
            this.Icon = icon;
            this.Destination = destination;
            this.Visible = visible;
            this.IsHeader = isHeader;
        }

        #endregion .Ctor

        
    }
}