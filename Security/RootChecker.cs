/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using Android.OS;
using Android.Util;
using Java.IO;
using Java.Lang;
using Console = System.Console;
using Exception = System.Exception;
using Process = Java.Lang.Process;

namespace QuasarTech.Xamarin.AndroidSupport.Security
{
    // Reference: http://stackoverflow.com/questions/1101380/determine-if-running-on-a-rooted-device
    public class RootChecker
    {
        private const string LOG_TAG = "ROOTCHECKER";

        public bool IsDeviceRooted()
        {
            bool isDeviceRooted = CheckRootMethod1() || CheckRootMethod2() || CanRunRootCommands();

            Log.Info(LOG_TAG, isDeviceRooted ? "WARNING: Root permissions detected." : "No root permissions detected");

            return isDeviceRooted;
        }

        public bool CheckRootMethod1()
        {
            string buildTags = Build.Tags;
            return buildTags != null && buildTags.Contains("test-keys");
        }

        public bool CheckRootMethod2()
        {
            try
            {
                File file = new File("/system/app/Superuser.apk");
                return file.Exists();
            }
            catch (Exception e)
            {
                Log.Error(LOG_TAG, e.StackTrace);
                return false;
            }
        }
        
        // Reference: http://forums.xamarin.com/discussion/21437/app-with-root-access
        public static bool CanRunRootCommands()
        {
            bool retval = false;
            try
            {
                Process suProcess = Runtime.GetRuntime().Exec("su");
                var os = new DataOutputStream(suProcess.OutputStream);
                var osRes = new DataInputStream(suProcess.InputStream);

                os.WriteBytes("id\n");
                os.Flush();

                string currUid = osRes.ReadLine();
                bool exitSu = false;

                if (null == currUid)
                {
                    Console.WriteLine("Can't get root access or denied by user");
                }
                else if (true == currUid.Contains("uid=0"))
                {
                    retval = true;
                    exitSu = true;
                    Console.WriteLine("Root access granted");
                }
                else
                {
                    exitSu = true;
                    Console.WriteLine("Root access rejected: " + currUid);
                }

                if (exitSu)
                {
                    os.WriteBytes("exit\n");
                    os.Flush();
                }
            }
            catch (Java.Lang.Exception e)
            {
                retval = false;
                Console.WriteLine("Root access rejected [" + e.Class.Name + "] : " + e.Message);
            }

            return retval;
        }
    }
}