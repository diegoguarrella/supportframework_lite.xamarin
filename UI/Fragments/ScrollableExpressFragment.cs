/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using Android.OS;
using Android.Views;
using QuasarTech.Xamarin.AndroidSupport.UI.Widgets;

namespace QuasarTech.Xamarin.AndroidSupport.UI.Fragments
{
    public abstract class ScrollableExpressFragment : AsyncTaskDependentFragment, IEnhancedScrollViewListener
    {
        protected EnhancedScrollView svContent;

        protected bool HasHeaderView = false;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            rootView = base.OnCreateView(inflater, container, savedInstanceState);
            
            svContent = rootView.FindViewById<EnhancedScrollView>(Resource.Id.svContent);
			if (svContent!=null) svContent.ScrollListener=this;

            if (HasHeaderView)
            {
                View headerView = rootView.FindViewById(Resource.Id.headerView);
                svContent.HeaderView = headerView;
            }
            return rootView;
        }

		public void UpdateActionBarBackground(EnhancedScrollView scrollView) {
			if (svContent!=null)
				ParentActivity.SetActionBarBackgroundAlpha(this.GetAlphaforActionBar(svContent.ScrollY));
		}

	    public void OnScrollChanged(EnhancedScrollView scrollView, int x, int y, int oldx, int oldy) {
			UpdateActionBarBackground(scrollView);
	    }
	
	    private int GetAlphaforActionBar(int scrollY) {
		    int minDist = 0, maxDist = 650;
		    if (scrollY > maxDist) {
			    return 255;
		    } else if (scrollY < minDist) {
			    return 0;
		    } else {
			    int alpha = 0;
			    alpha = (int) ((255.0 / maxDist) * scrollY);
			    return alpha;
		    }
	    }

		public override void OnResume ()
		{
			base.OnResume ();
			UpdateActionBarBackground (svContent);
		}
    }
}