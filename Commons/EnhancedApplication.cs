using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Webkit;
using Calligraphy;
using CrossplatformCommons.ControlFlow;
using CrossplatformCommons.Networking;
using QuasarTech.Xamarin.AndroidSupport.Commons;
using QuasarTech.Xamarin.AndroidSupport.Security;
using QuasarTech.Xamarin.AndroidSupport.UI;
using QuasarTech.Xamarin.AndroidSupport.UI.Fragments;
using Environment = System.Environment;

namespace QuasarTech.Xamarin.AndroidSupport.Commons
{
    public class EnhancedApplication : Application
    {
        private bool _isDeviceRooted;
        private RootChecker rootWatchdog;
        
        public EnhancedApplication(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer)
        {
        }

        public bool IsDeviceRooted
        {
            get { return rootWatchdog.IsDeviceRooted(); }
        }

        public override void OnCreate()
        {
            base.OnCreate();
			CalligraphyConfig.InitDefault (new CalligraphyConfig.Builder ()
				.SetDefaultFontPath ("fonts/roboto_regular.ttf")
				.SetFontAttrId (Resource.Attribute.fontPath)
				.Build ());
            
            // any work here is likely to be blocking (static constructors run on whatever thread that first
            // access its instance members, which in our case is an activity doing an initialization check),
            // so we want to do it on a background thread
            new Task(() =>
            {
                rootWatchdog = new RootChecker();
                //_isDeviceRooted = rootWatchdog.IsDeviceRooted();*/
                
                CookieSyncManager.CreateInstance(Context);

                Log.Debug(this.Class.Name, "App initialized.");
            }).Start();
        }
        
        
        public string GetAppVersion()
        {
            PackageInfo pInfo = this.PackageManager.GetPackageInfo(this.PackageName, 0);
            return "(" + pInfo.VersionCode + ") - " + pInfo.VersionName;
        }
    }
}