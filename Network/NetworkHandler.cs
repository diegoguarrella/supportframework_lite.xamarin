/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using Android.Content;
using Android.Net;

namespace QuasarTech.Xamarin.AndroidSupport.Network
{
    public class NetworkHandler : BroadcastReceiver
    {
        private INetworkListener listener;
        private ConnectivityManager connMgr;

        public NetworkHandler(ConnectivityManager connMgr)
            : base()
        {
            this.connMgr = connMgr;
        }

        public Boolean IsDataConnected()
        {
            try
            {
                return connMgr.ActiveNetworkInfo.IsConnectedOrConnecting;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public override void OnReceive(Context arg0, Intent arg1)
        {
            if (listener != null)
                listener.UpdateData(IsDataConnected());
        }

        public void SetListener(INetworkListener listener)
        {
            this.listener = listener;
            if (listener != null)
                listener.UpdateData(IsDataConnected());
        }
    }

    public interface INetworkListener
    {
        void UpdateData(bool dataConnected);
    }
}