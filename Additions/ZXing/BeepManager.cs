﻿using System;
using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Media;
using Android.OS;

namespace QuasarTech.Xamarin.AndroidSupport.Additions.ZXing
{
	public class BeepManager {

		private static string TAG              = "BeepManager";

		private static float  BEEP_VOLUME      = 0.10f;
		private static long   VIBRATE_DURATION = 200L;

		private Activity      activity;
		private MediaPlayer         mediaPlayer;
		private bool             playBeep = true;
		private bool             vibrate = true;

		public BeepManager(Activity activity) {
			this.activity = activity;
			this.mediaPlayer = null;
			UpdatePrefs();
		}

		void UpdatePrefs() {
			playBeep = ShouldBeep(activity);
			if (playBeep && mediaPlayer == null) {
				// The volume on STREAM_SYSTEM is not adjustable, and users found it too loud,
				// so we now play on the music stream.
				activity.VolumeControlStream = Stream.Music;
				mediaPlayer = BuildMediaPlayer(activity);
			}
		}

		public void PlayBeepSoundAndVibrate() {
			if (playBeep && mediaPlayer != null) {
				mediaPlayer.Start();
			}
			if (vibrate) {
				Vibrator vibrator = (Vibrator) activity.GetSystemService(Context.VibratorService);
				vibrator.Vibrate(VIBRATE_DURATION);
			}
		}

		private static bool ShouldBeep(Context activity) {
			
				// See if sound settings overrides this
				AudioManager audioService = (AudioManager) activity.GetSystemService(Context.AudioService);
				
			return (audioService.RingerMode == RingerMode.Normal);
		}

		private static MediaPlayer BuildMediaPlayer(Context activity) {
			MediaPlayer mediaPlayer = new MediaPlayer();
			mediaPlayer.SetAudioStreamType(Stream.Music);
			// When the beep has finished playing, rewind to queue up another one.
			mediaPlayer.Completion += (object sender, EventArgs e) => {
				mediaPlayer.SeekTo(0);
			};

			AssetFileDescriptor file = activity.Resources.OpenRawResourceFd(Resource.Raw.zxinglib_beep);
			try {
				mediaPlayer.SetDataSource(file.FileDescriptor, file.StartOffset, file.Length);
				file.Close();
				mediaPlayer.SetVolume(BEEP_VOLUME, BEEP_VOLUME);
				mediaPlayer.Prepare();
			} catch (Exception e) {
				Console.WriteLine(TAG + e.StackTrace);
				mediaPlayer = null;
			}
			return mediaPlayer;
		}

	}
}

