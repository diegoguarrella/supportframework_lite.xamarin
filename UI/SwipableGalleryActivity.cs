/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content.PM;
using Android.Content.Res;
using Android.OS;
using Android.Support.V4.View;
using Android.Views;
using Android.Widget;
using QuasarTech.Xamarin.AndroidSupport.UI.Adapters;
using ActionBar = Android.Support.V7.App.ActionBar;
using AndroidResource = Android.Resource;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace QuasarTech.Xamarin.AndroidSupport.UI
{
	[Activity(Label = "SwipableGalleryActivity", Theme = "@style/AppTheme", ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize | ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden, ScreenOrientation = ScreenOrientation.Portrait, HardwareAccelerated = true)]
	public class SwipableGalleryActivity : EnhancedActivity
    {
        private List<string> imageURLs;
        private string mTitle;

        private ViewPager vPager;

        protected override View MakeMenuView(Bundle savedInstanceState)
        {
            return null;
        }
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            // this.SetRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            base.OnCreate(savedInstanceState);

            Window.AddFlags(WindowManagerFlags.KeepScreenOn);
            this.SupportRequestWindowFeature((int)WindowFeatures.ActionBarOverlay);

            SetContentView(Resource.Layout.act_gallery);

            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            if (toolbar != null)
            {
                SetSupportActionBar(toolbar);
            }

            ActionBar aBar = this.SupportActionBar;
            aBar.SetHomeButtonEnabled(true);

            aBar.SetBackgroundDrawable(Resources.GetDrawable(
                Resource.Drawable.actionbar_overlay_bg));

            aBar.SetDisplayHomeAsUpEnabled(true);
            aBar.SetDisplayShowTitleEnabled(true);

            aBar.SetTitle(Resource.String.section_image_gallery);
            aBar.SetSubtitle(Resource.String.app_name);

            vPager = FindViewById<ViewPager>(Resource.Id.vpImages);

			imageURLs = (this.Intent.GetStringArrayListExtra("entryList") ?? new List<string>()).ToList();
        }

        protected override void OnDestroy()
        {
            Initialized = false;
            base.OnDestroy();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
			if (item.ItemId == AndroidResource.Id.Home)
            {
                Finish();
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        protected override void OnResume()
        {
            base.OnResume();
            if (!Initialized)
            {
                Initialized = true;

                vPager.Adapter = new ImagePagerAdapter(this, imageURLs);
            }
        }

        public void SetTitle(string title)
        {
            mTitle = title;
            this.SupportActionBar.Title = mTitle;
        }

        public void SwitchActionBar(bool custom, string title)
        {
            ActionBar actionBar = this.SupportActionBar;
            actionBar.SetDisplayShowCustomEnabled(custom);
            if (custom)
            {
                actionBar.SetCustomView(Resource.Layout.custom_actionbar);
                actionBar.SetDisplayHomeAsUpEnabled(true);
                actionBar.SetIcon(null);
                FindViewById<TextView>(Resource.Id.action_bar_title).Text = title;
            }
            else
            {
                actionBar.Title = title;
                actionBar.SetIcon(Resource.Drawable.ic_launcher);
                actionBar.SetDisplayHomeAsUpEnabled(false);
                actionBar.SetDisplayShowHomeEnabled(true);
            }
        }

        public void OnPageScrollStateChanged(int state)
        {
        }

        public void OnPageScrolled(int position, float positionOffset, int positionOffsetPixels)
        {
        }

        public void OnPageSelected(int position)
        {
        }

        public override void OnClick(View v)
        {
            string tag = (string)v.Tag;

            if (tag == null)
                return;

            if (tag.Equals("BTN_CLOSE"))
            {
                this.Finish();
            }
        }
    }
}