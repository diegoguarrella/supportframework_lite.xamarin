/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using Android.Content;
using Android.Graphics.Drawables;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace QuasarTech.Xamarin.AndroidSupport.UI.Widgets {

public class AnimatedImageView : ImageView {
    
	
	public AnimatedImageView(Context context) : base (context) { }
        
	public AnimatedImageView(Context context, IAttributeSet attrs) : base (context, attrs) { }
        
	public AnimatedImageView(Context context, IAttributeSet attrs, int defStyle): base (context, attrs, defStyle) { }

	private void UpdateAnimationsState() {
		bool running = this.Visibility ==  ViewStates.Visible && HasWindowFocus;
		UpdateAnimationState(this.Drawable, running);
		UpdateAnimationState(this.Background, running);
	}

	private void UpdateAnimationState(Drawable drawable, bool running) {
		if (drawable is AnimationDrawable) {
			AnimationDrawable animationDrawable = (AnimationDrawable) drawable;
			if (running) {
				animationDrawable.Start();
			} else {
				animationDrawable.Stop();
			}
		}
	}

	protected override void OnVisibilityChanged(View changedView, ViewStates visibility) {
		base.OnVisibilityChanged(changedView, visibility);
		UpdateAnimationsState();
	}

	public override void OnWindowFocusChanged(bool hasWindowFocus) {
		base.OnWindowFocusChanged(hasWindowFocus);
		UpdateAnimationsState();
	}
}}