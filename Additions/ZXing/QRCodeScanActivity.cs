﻿/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using QuasarTech.Xamarin.AndroidSupport.UI;
using ZXing;
using ZXing.Mobile;
using AndroidResource = Android.Resource;
using Result = Android.App.Result;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace QuasarTech.Xamarin.AndroidSupport.Additions.ZXing
{
    [Activity(Label = "QRCodeScanActivity", Theme = "@style/AppTheme", ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize | ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden, ScreenOrientation = ScreenOrientation.Portrait, HardwareAccelerated = true)]
    public class QRCodeScanActivity : EnhancedActivity
    {
        private ZXingScannerFragment scanFragment;

        private bool flashlightOn = false;
        private bool hasFlashlight = false;
        private bool preventUpdates;
        private int tipCounter = 0;
        private string[] tips;

		BeepManager beepManager;

        private TextView tvStatus;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            this.RequestedOrientation = ScreenOrientation.Portrait;
            base.OnCreate(savedInstanceState);

            this.Window.AddFlags(WindowManagerFlags.KeepScreenOn);
            this.SupportRequestWindowFeature((int)WindowFeatures.ActionBarOverlay);

            SetContentView(Resource.Layout.act_qrscanner);

            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            if (toolbar != null)
                SetSupportActionBar(toolbar);

            var aBar = this.SupportActionBar;
            aBar.Title = GetString(Resource.String.act_title_qrscanner);
            aBar.Subtitle = GetString(Resource.String.act_subtitle);
            
			aBar.SetHomeButtonEnabled(true);
            aBar.SetDisplayHomeAsUpEnabled(true);
            aBar.SetBackgroundDrawable(Resources.GetDrawable(Resource.Drawable.actionbar_overlay_bg));

            hasFlashlight = PackageManager.HasSystemFeature(PackageManager.FeatureCameraFlash);

            OnCreateStep2();

            tvStatus = FindViewById<TextView>(Resource.Id.tvStatus);

            tips = this.Resources.GetStringArray(Resource.Array.tips);
            tvStatus.Text = tips[0];

			beepManager = new BeepManager (this);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            if (hasFlashlight)
                MenuInflater.Inflate(Resource.Menu.qrscanner, menu);

            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int itemId = item.ItemId;

            if (itemId == AndroidResource.Id.Home)
            {
                Finish();
                return true;
            }
            else if (itemId == Resource.Id.action_flashlight)
            {
                SwitchFlashlight();
                return true;
            }
            else
                return base.OnOptionsItemSelected(item);
        }

        protected override void OnPause()
        {
            base.OnPause();
            preventUpdates = true;
        }

        protected override void OnResume()
        {
            base.OnResume();
            preventUpdates = false;

            PerformScan();

            this.Handler.PostDelayed(UpdateTips, 4000);
        }

        private void UpdateTips()
        {
            tipCounter = (++tipCounter) % tips.Length;
            if (!preventUpdates)
            {
                tvStatus.Text = tips[tipCounter];
                this.Handler.PostDelayed(UpdateTips, 4000);
            }
        }
        
        protected void SwitchFlashlight()
        {
            if (hasFlashlight)
            {
                scanFragment.ToggleTorch();
                flashlightOn = !flashlightOn;
                
                Toast.MakeText(this,
                    this.GetString(flashlightOn ? Resource.String.flashlight_on : Resource.String.flashlight_off),ToastLength.Short).Show();
            }
        }

        protected override View MakeMenuView(Bundle savedInstanceState)
        {
            return null;
        }

        public override void OnClick(View v)
        {
            if (IsAccidentalDoubleTapDetected())
                return;

            if (currentFragment != null)
                currentFragment.OnClick(v);
        }

        private void OnCreateStep2()
        {
            scanFragment = new ZXingScannerFragment();
            var viewfinder = new AlternativeViewFinder(this) {HasLaserEffect = true};

            scanFragment.CustomOverlayView = viewfinder;
			scanFragment.UseCustomOverlayView = true;

            SupportFragmentManager.BeginTransaction()
                .Replace(Resource.Id.fragment_container, scanFragment, "ZXINGFRAGMENT")
                .Commit();
        }

        private void PerformScan()
        {

            var opts = new MobileBarcodeScanningOptions
            {
                PossibleFormats = new List<BarcodeFormat> {
                    BarcodeFormat.QR_CODE
                },
                CameraResolutionSelector = availableResolutions => {

                    foreach (var ar in availableResolutions)
                    {
                        Console.WriteLine("Resolution: " + ar.Width + "x" + ar.Height);
                    }
                    return null;
                },
                DelayBetweenContinuousScans = int.MaxValue
            };

            scanFragment.StartScanning(result =>
            {
				beepManager.PlayBeepSoundAndVibrate();
                scanFragment.StopScanning();

                Intent returnIntent = new Intent();

                // Null result means scanning was cancelled
                if (result == null || string.IsNullOrEmpty(result.Text))
                {
                    returnIntent.PutExtra("result", result.Text);
                    SetResult(Result.Canceled, returnIntent);
                    Finish();

                    //Toast.MakeText (this, "Scanning Cancelled", ToastLength.Long).Show ();
                    return;
                }

                // Otherwise, proceed with result
                returnIntent.PutExtra("result", result.Text);
                SetResult(Result.Ok, returnIntent);
                Finish();

                //RunOnUiThread (() => Toast.MakeText (this, "Scanned: " + result.Text, ToastLength.Short).Show ());
            },opts);
        }

        public static readonly int REQUEST_CODE = 0x0000c0de;
    }
}