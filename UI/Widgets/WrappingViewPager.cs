/*
 * Based on code listed at: https://stackoverflow.com/questions/8394681/android-i-am-unable-to-have-viewpager-wrap-content 
 *
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using Android.Content;
using Android.Runtime;
using Android.Support.V4.View;
using Android.Util;
using Android.Views;

namespace QuasarTech.Xamarin.AndroidSupport.UI.Widgets
{
	public class WrappingViewPager : ViewPager {
		
		private bool IsSettingHeight=false;

		public WrappingViewPager (Context context) : base (context) {
		}
		public WrappingViewPager (Context context, IAttributeSet attrs) : base (context, attrs) {
		}
		public WrappingViewPager(IntPtr javaReference, JniHandleOwnership ownership) : base(javaReference, ownership) {
		}

		protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec) {						
		    base.OnMeasure(widthMeasureSpec, heightMeasureSpec);
			if (this.LayoutParameters.Height == ViewGroup.LayoutParams.WrapContent) {
		        // find the first child view
		        View view = GetChildAt(0);
		        if (view != null) {
		            // measure the first child view with the specified measure spec
		            view.Measure(widthMeasureSpec, heightMeasureSpec);
		            int h = view.MeasuredHeight;
		            SetMeasuredDimension(MeasuredWidth, h);
		            //do not recalculate height anymore
					this.LayoutParameters.Height = h;
		        }
		    }
		}
		
		public void SetVariableHeight()
		{
		    // super.measure() calls finishUpdate() in adapter, so need this to stop infinite loop
		    if (!this.IsSettingHeight)
		    {
		        this.IsSettingHeight = true;

		        int maxChildHeight = 0;
				int widthMeasureSpec = MeasureSpec.MakeMeasureSpec(MeasuredWidth, MeasureSpecMode.Exactly);

				View child = GetChildAt(this.CurrentItem);
                
		        if (child!=null) {
					child.Measure(widthMeasureSpec, MeasureSpec.MakeMeasureSpec(ViewGroup.LayoutParams.WrapContent, MeasureSpecMode.Unspecified));
		            maxChildHeight = child.MeasuredHeight > maxChildHeight ? child.MeasuredHeight : maxChildHeight;	        
		        } else { 
					for (int i = 0; i < this.ChildCount; i++)
			        {
			            child = GetChildAt(i);
			            if (child==null)
			            	continue;
						child.Measure(widthMeasureSpec, MeasureSpec.MakeMeasureSpec(ViewGroup.LayoutParams.WrapContent, MeasureSpecMode.Unspecified));
			            maxChildHeight = child.MeasuredHeight > maxChildHeight ? child.MeasuredHeight : maxChildHeight;
			        }   
		        }
		            
		        int height = maxChildHeight + PaddingTop + PaddingBottom;
				int heightMeasureSpec = MeasureSpec.MakeMeasureSpec(height, MeasureSpecMode.Exactly);

		        base.Measure(widthMeasureSpec, heightMeasureSpec);
		        RequestLayout();

		        this.IsSettingHeight = false;
		    }
		}
	}
}
