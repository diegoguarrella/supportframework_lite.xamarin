/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Util;
using Android.Views;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace QuasarTech.Xamarin.AndroidSupport.UI
{

    public abstract class EnhancedFragment : Fragment, View.IOnClickListener
    {
        protected View rootView;
        protected int layoutResId;
        protected int menuResId;
        protected int titleResId;
        protected int actionBarBackgroundColorId;
        protected ScreenOrientation screenOrientation;

        protected bool Initialized = false;

        public bool IsLandscapeAllowed = false;
        
        public Handler mHandler { get; protected set; }

        protected EnhancedActivity ParentActivity;

        public int NavigationDepth = 0;
        public bool NavigationReset = false;
        public bool HasMenu = false;

        public string Title, SubTitle;

        protected EnhancedFragment()
            : base()
        {
            this.Arguments = new Bundle();
        }

        public FragmentManager GetFragmentManager()
        {
            return Activity.SupportFragmentManager;
        }

        [Obsolete("deprecated")]
        public override void OnAttach(Activity activity)
        {
            base.OnAttach(activity);
            Log.Verbose("SupportFramework LITE Library", "(c)2012-2016 Diego Guarrella - Released under Apache 2.0 free software license");
            ParentActivity = (EnhancedActivity)activity;
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            this.HasOptionsMenu = true;

            mHandler = ParentActivity.Handler;
        }

        public abstract void OnClick(View v);

        public override void OnDestroy()
        {
            base.OnDestroy();
            // TODO: Check if really effective on real device
            Log.Debug(this.Class.Name, "ON DESTROY: GC Collection requested.");
            GC.Collect(0);
        }

        public override void OnLowMemory()
        {
            base.OnLowMemory();
            // TODO: Check if really effective on real device
            Log.Debug(this.Class.Name, "LOW MEMORY: GC Collection requested.");
            GC.Collect();
        }

        protected bool IsAccidentalDoubleTapDetected()
        {
            return ParentActivity.IsAccidentalDoubleTapDetected();
        }

        public void SetTitle(int resId)
        {
            this.Title = Resources.GetString(resId);
            ParentActivity.UpdateTitle(this.Title);
        }
        
    }
}