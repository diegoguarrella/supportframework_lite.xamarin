/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using Android.Content;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Util;
using Android.Views;
using Calligraphy;
using Java.Text;
using Java.Util;
/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using QuasarTech.Xamarin.AndroidSupport.Utils;

namespace QuasarTech.Xamarin.AndroidSupport.UI
{
    public abstract class EnhancedActivity : AppCompatActivity,
            Handler.ICallback, View.IOnClickListener
    {
        protected bool Initialized = false;

        public enum ORIENTATION
        {
            LANDSCAPE, PORTRAIT
        }

        public static readonly long DOUBLE_CLICK_TIME_DELTA = 600;// milliseconds
        public long lastClickTime = 0;
        private ColorDrawable aBarBackgroundColor;

        public EnhancedFragment currentFragment;

        protected DateFormat dateFormat = DateFormat.GetDateInstance(DateFormat.Short,
                Locale.Default);

        public DrawerLayout mDrawerLayout;

        private Handler mHandler;

        public Handler Handler
        {
            get { return mHandler ?? (mHandler = new Handler(Looper.MainLooper, this)); }
        }

        private Stack<EnhancedFragment> navigationStack;

        private ORIENTATION screenOrientation = ORIENTATION.PORTRAIT;

        protected DateFormat timeFormat = DateFormat.GetTimeInstance(DateFormat.Medium,
                Locale.Default);

        public void CloseDrawers()
        {
            mDrawerLayout.CloseDrawers();
        }

        protected ActionBar aBar;

        protected abstract View MakeMenuView(Bundle savedInstanceState);

        public override void OnBackPressed()
        {
			if (this.mDrawerLayout != null && this.mDrawerLayout.IsDrawerOpen (GravityCompat.Start)) {
				CloseDrawers ();
				return;
			}

            if (navigationStack.Count > 1)
            {
				navigationStack.Pop();
				ReplaceFragment(navigationStack.Peek());
            }
            else
            {
				if (navigationStack.Count == 1)
					RemoveFragment(navigationStack.Pop());
				navigationStack.Clear();
				base.OnBackPressed();    
			}
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds); 

            Log.Verbose("SupportFramework LITE Library",
                    "(c)2012-2016 Diego Guarrella - Released under Apache 2.0 free software license");
            navigationStack = new Stack<EnhancedFragment>();
        }

        public void SetActionBarBackgroundColor(int resId)
        {
            aBarBackgroundColor = new ColorDrawable(this.Resources.GetColor(
                    resId));
            this.SupportActionBar.SetBackgroundDrawable(aBarBackgroundColor);
        }

        public void SetActionBarBackgroundAlpha(int alpha)
        {
            if (aBarBackgroundColor != null)
                aBarBackgroundColor.SetAlpha(alpha);
        }
        public void RemoveFragment(EnhancedFragment fragment)
        {
            FragmentTransaction ft = SupportFragmentManager.BeginTransaction();
            ft.Remove(fragment);
            ft.Commit();
        }
        
        public virtual void ReplaceFragment(EnhancedFragment fragment, bool forceReset=false)
        {
            Log.Debug("Activity #" + this.GetHashCode(), "ReplaceFragment requested");

            FragmentManager fm = this.SupportFragmentManager;
            FragmentTransaction ft = fm.BeginTransaction();

            if (forceReset || fragment.NavigationReset)
            {
                for (int i = 0; i < fm.BackStackEntryCount; ++i)
                    fm.PopBackStack();
                for (int i = navigationStack.Count; i > 1; i--)
                    ft.Remove(navigationStack.Pop());
            }


            ft.Replace(Resource.Id.content_frame, fragment);
            ft.SetTransition(FragmentTransaction.TransitFragmentFade);

            if (currentFragment == null || (navigationStack.Count > 0 && navigationStack.Peek() != fragment))
                navigationStack.Push(fragment);

            currentFragment = fragment;

            ft.CommitAllowingStateLoss();
            fm.ExecutePendingTransactions();
        }

		public void SetIcon(int iconId)
        {
            SupportActionBar.SetIcon(iconId);
        }

        public void SwitchOrientation(ORIENTATION o)
        {
            if (currentFragment == null || !currentFragment.IsLandscapeAllowed)
                return;

            WindowManagerLayoutParams attrs = Window.Attributes;

            screenOrientation = o;

            if (o == ORIENTATION.PORTRAIT)
            {
                SupportActionBar.Show();
                attrs.Flags &= (~WindowManagerFlags.Fullscreen);
                Window.Attributes = attrs;
            }
            else
            {
                SupportActionBar.Hide();
                attrs.Flags |= WindowManagerFlags.Fullscreen;
                Window.Attributes = attrs;
            }
        }

        public void UpdateTitle(string subtitle)
        {
            SupportActionBar.Title = subtitle;
            SupportActionBar.SetSubtitle(Resource.String.app_name);
            SupportActionBar.SetIcon(Resource.Drawable.ic_launcher);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(true);
        }

        
        protected override void AttachBaseContext(Context newBase) {
			base.AttachBaseContext(CalligraphyContextWrapper.Wrap(newBase));
        }

        public virtual bool HandleMessage(Message msg)
        {
            return false;
        }

        public abstract void OnClick(View v);

        public bool IsAccidentalDoubleTapDetected()
        {
            long clickTime = CommonUtils.GetTimestamp();
            
            if (clickTime - lastClickTime <= DOUBLE_CLICK_TIME_DELTA)
                return true;

            lastClickTime = clickTime;

            return false;
        }
    }
}