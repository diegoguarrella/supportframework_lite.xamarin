/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Threading.Tasks;
using Android.OS;
using Android.Views;
using Android.Widget;
using CrossplatformCommons.ControlFlow;
using CrossplatformCommons.Networking;
using OperationCanceledException = System.OperationCanceledException;

namespace QuasarTech.Xamarin.AndroidSupport.UI.Fragments
{
    public abstract class AsyncTaskDependentFragment : ExpressFragment, IAsyncTaskDependentObject
    {
        protected LinearLayout llNoData;
        protected LinearLayout llTimeout;
        protected LinearLayout llError;
        protected LinearLayout llLoading;
	    protected View vContent;

        public virtual void OnDataResult(object data)
        {
            llLoading.Visibility = ViewStates.Gone;
            if (llError != null) llError.Visibility = ViewStates.Gone;
            if (llTimeout != null) llTimeout.Visibility = ViewStates.Gone;
            if (vContent != null) 
                vContent.Visibility = ViewStates.Visible;
        }

        public virtual void UIOnRequestCanceled(CancelationReason reason)
        {
            llLoading.Visibility = ViewStates.Gone;
            if (llError != null) llError.Visibility = ViewStates.Visible;
            if (vContent != null)
                vContent.Visibility = ViewStates.Gone;
        }

        public virtual void UIOnRequestError(CancelationReason reason)
        {
            llLoading.Visibility = ViewStates.Gone;
            if (reason == CancelationReason.NETWORK_TIMEOUT || reason == CancelationReason.RENDERING_TIMEOUT) {
                if (llTimeout != null) llTimeout.Visibility = ViewStates.Visible;
                if (llError != null) llError.Visibility = ViewStates.Gone;
            }
            else
            {
                if (llTimeout != null) llTimeout.Visibility = ViewStates.Gone;
                if (llError != null) llError.Visibility = ViewStates.Visible;
            }
            
            if (vContent != null)
                vContent.Visibility = ViewStates.Gone;
        }

        public virtual void UIOnRequestStart()
        {
            llLoading.Visibility = ViewStates.Visible;
            if (llTimeout != null) llTimeout.Visibility = ViewStates.Gone;
            if (llError != null) llError.Visibility = ViewStates.Gone;
            if (vContent != null) vContent.Visibility = ViewStates.Gone;
        }
        
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            llNoData = rootView.FindViewById<LinearLayout>(Resource.Id.llNoData);
            llError = rootView.FindViewById<LinearLayout>(Resource.Id.llError);
            llLoading = rootView.FindViewById<LinearLayout>(Resource.Id.llLoading);
            llTimeout = rootView.FindViewById<LinearLayout>(Resource.Id.llTimeout);

            return rootView;
        }

        protected virtual async Task<bool> RefreshData()
        {
            return await ExecuteOfflineDataRequest();
        }

        protected async Task<bool> ExecuteOfflineDataRequest()
        {

            {
                UIOnRequestStart();
                //CleanData ();

                try
                {
                    var data = await PerformDataRequest();

                    OnDataResult(data);

                    return true;
                }
                catch (OperationCanceledException e)
                {
                    UIOnRequestError(CancelationReason.UNKNOWN);

                    return false;
                }
            }
        }

        public abstract void CleanData ();

        public abstract Task<object> PerformDataRequest();


    }
}