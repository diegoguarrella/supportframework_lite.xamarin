package eu.quasartech.android.support.ui.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class WrappingViewPager extends ViewPager {
	
	private boolean isSettingHeight=false;

	public WrappingViewPager(Context context) {
		super(context);
	}
	public WrappingViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	    if (getLayoutParams().height == ViewGroup.LayoutParams.WRAP_CONTENT) {
	        // find the first child view
	        View view = getChildAt(0);
	        if (view != null) {
	            // measure the first child view with the specified measure spec
	            view.measure(widthMeasureSpec, heightMeasureSpec);
	            int h = view.getMeasuredHeight();
	            setMeasuredDimension(getMeasuredWidth(), h);
	            //do not recalculate height anymore
	            getLayoutParams().height = h;
	        }
	    }
	}
	
	public void setVariableHeight()
	{
	    // super.measure() calls finishUpdate() in adapter, so need this to stop infinite loop
	    if (!this.isSettingHeight)
	    {
	        this.isSettingHeight = true;

	        int maxChildHeight = 0;
	        int widthMeasureSpec = MeasureSpec.makeMeasureSpec(getMeasuredWidth(), MeasureSpec.EXACTLY);
	        //for (int i = 0; i < getChildCount(); i++)
	        //{
	            View child = getChildAt(this.getCurrentItem());
	            //}

	        if (child!=null) {
	        	child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(ViewGroup.LayoutParams.WRAP_CONTENT, MeasureSpec.UNSPECIFIED));
	            maxChildHeight = child.getMeasuredHeight() > maxChildHeight ? child.getMeasuredHeight() : maxChildHeight;	        
	        } else { 
	            for (int i = 0; i < getChildCount(); i++)
		        {
		            child = getChildAt(i);
		            if (child==null)
		            	continue;
		            child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(ViewGroup.LayoutParams.WRAP_CONTENT, MeasureSpec.UNSPECIFIED));
		            maxChildHeight = child.getMeasuredHeight() > maxChildHeight ? child.getMeasuredHeight() : maxChildHeight;
		        }   
	        }
	            
	        int height = maxChildHeight + getPaddingTop() + getPaddingBottom();
	        int heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);

	        super.measure(widthMeasureSpec, heightMeasureSpec);
	        requestLayout();

	        this.isSettingHeight = false;
	    }
	}
	/*
    public void resize(int position) {
        View view = this
        if (view == null) 
            return;
        view.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        int width = view.getMeasuredWidth();
        int height = view.getMeasuredHeight();
            //The layout params must match the parent of the ViewPager 
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width , height); 
        this.setLayoutParams(params);
    }*/
}
