﻿/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections;
using Android.Content;
using Android.Database;
using Android.Views;
using Android.Widget;

namespace QuasarTech.Xamarin.AndroidSupport.UI.Adapters
{
    public abstract class EnhancedArrayAdapter : ArrayAdapter
    {
        /* Common LayoutInflater */
        protected LayoutInflater inflater;
        /* Current context */
        protected Context context;

        protected EnhancedArrayAdapter(Context context, int textViewResourceId, IList objects)
            : base(context, textViewResourceId, objects)
        {
            // Initialize the common inflater
            inflater = LayoutInflater.From(context);
        }

        /* Returns the number of items loaded */

        public abstract int GetCount();

        /* Returns the item at the given position */

        public abstract new Object GetItem(int position);

        /* Returns the item id at the given position */

        public abstract new long GetItemId(int position);

        public override void UnregisterDataSetObserver(DataSetObserver observer)
        {
            if (observer != null)
            {
                base.UnregisterDataSetObserver(observer);
            }
        }
    }
}