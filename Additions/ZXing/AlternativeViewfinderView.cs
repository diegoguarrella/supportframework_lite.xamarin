﻿/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Views;
using ZXing;

namespace QuasarTech.Xamarin.AndroidSupport.Additions.ZXing
{

	public class AlternativeViewFinder : View{

		protected static int[] SCANNER_ALPHA = {0, 64, 128, 192, 255, 192, 128, 64};
		protected static long ANIMATION_DELAY = 80L;
		protected static int CURRENT_POINT_OPACITY = 0xA0;
		protected static int MAX_RESULT_POINTS = 20;

		private Paint paint;
		private Bitmap resultBitmap;
		private Color maskColor;
		private Color resultColor;
		private Color frameColor;
		private Color laserColor;
		private Color resultPointColor;
		private int scannerAlpha;
		private List<ResultPoint> possibleResultPoints;
		private List<ResultPoint> lastPossibleResultPoints;

		public AlternativeViewFinder(Context context) : base(context){
			
			// Initialize these once for performance rather than calling them every time in onDraw().
			paint = new Paint(PaintFlags.AntiAlias);

			maskColor = Resources.GetColor(Resource.Color.zxinglib_viewfinder_mask);
			resultColor = Resources.GetColor(Resource.Color.zxinglib_result_view);
			frameColor = Resources.GetColor(Resource.Color.zxinglib_viewfinder_frame);
			laserColor = Resources.GetColor(Resource.Color.zxinglib_viewfinder_laser);
			resultPointColor = Resources.GetColor(Resource.Color.zxinglib_possible_result_points);
			scannerAlpha = 0;
			possibleResultPoints = new List<ResultPoint>(5);
			lastPossibleResultPoints = null;
		}

		public static Bitmap GetMaskedBitmap(Android.Content.Res.Resources res, Bitmap source, Bitmap mask, int x, int y) {
			BitmapFactory.Options options = new BitmapFactory.Options();
			if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Honeycomb) {
				options.InMutable = true;
			}
			options.InPreferredConfig = Bitmap.Config.Argb8888;
			//Bitmap source = GetNinePatchBitmap(sourceResId, x, y, res);
			Bitmap bitmap;
			if (source.IsMutable) {
				bitmap = source;
			} else {
				bitmap = source.Copy(Bitmap.Config.Argb8888, true);
				source.Recycle();
			}
			bitmap.HasAlpha=true;
			Canvas canvas = new Canvas(bitmap);

			Paint paint = new Paint();
			paint.SetXfermode(new PorterDuffXfermode(PorterDuff.Mode.DstIn));
			canvas.DrawBitmap(mask, 0, 0, paint);
			mask.Recycle();
			return bitmap;
		}

		public static Bitmap GetNinePatchBitmap(int id,int x, int y, Android.Content.Res.Resources res){
			// id is a resource id for a valid ninepatch

			Bitmap bitmap = BitmapFactory.DecodeResource(res, id);

			byte[] chunk = bitmap.GetNinePatchChunk();
			NinePatchDrawable np_drawable = new NinePatchDrawable(bitmap, chunk, new Rect(), null);
			np_drawable.SetBounds(0, 0,x, y);

			Bitmap output_bitmap = Bitmap.CreateBitmap(x, y, Bitmap.Config.Argb8888);
			Canvas canvas = new Canvas(output_bitmap);
			np_drawable.Draw(canvas);

			return output_bitmap;
		}
        
		public bool HasLaserEffect = false;

		protected override void OnDraw(Canvas canvas) {
			if (this.IsInEditMode)
				return;
			
			Rect frame =  GetFramingRect(canvas); 
			if (frame == null) {
				return;
			}
			int width = canvas.Width;
			int height = canvas.Height;

			// Draw the exterior (i.e. outside the framing rect) darkened
			paint.Color= resultBitmap != null ? resultColor : maskColor;
			canvas.DrawRect(0, 0, width, frame.Top, paint);
			canvas.DrawRect(0, frame.Top, frame.Left, frame.Bottom + 1, paint);
			canvas.DrawRect(frame.Right + 1, frame.Top, width, frame.Bottom + 1,paint);
			canvas.DrawRect(0, frame.Bottom + 1, width, height, paint);

			if (resultBitmap != null) {
				// Draw the opaque result bitmap over the scanning rectangle
				paint.Alpha=CURRENT_POINT_OPACITY;
				//Bitmap mask = GetNinePatchBitmap(R.drawable.camera_target_mask2, frame.Width+5, frame.Height+5, Resources);

				//Bitmap finalResultBitmap = getMaskedBitmap(Resources, resultBitmap, mask, frame.Width, frame.Height);


				canvas.DrawBitmap(resultBitmap, null, new Rect(frame.Left+5,frame.Top+5, frame.Right-5, frame.Bottom-5), paint);
				// canvas.drawBitmap(resultBitmap, null, frame, paint);
				// You can change original image here and draw anything you want to
				// be masked on it.

				NinePatchDrawable bg = (NinePatchDrawable) Resources.GetDrawable(Resource.Drawable.camera_target_over_detected);
				if (bg != null) {
					bg.SetBounds(frame.Left, frame.Top, frame.Right + 1,
						frame.Bottom + 1);
					bg.Draw(canvas);
				}

			} else {

				NinePatchDrawable bg = (NinePatchDrawable) Resources.GetDrawable(Resource.Drawable.camera_target_over_scan);
				if (bg != null) {
					bg.SetBounds(frame.Left, frame.Top, frame.Right + 1,
						frame.Bottom + 1);
					bg.Draw(canvas);
				}

				// Draw a red "laser scanner" line through the middle to show
				// decoding is active
				if (HasLaserEffect) {
					paint.Color=laserColor;
					paint.Alpha=SCANNER_ALPHA[scannerAlpha];
					scannerAlpha = (scannerAlpha + 1) % SCANNER_ALPHA.Length;
					int middle = frame.Height() / 2 + frame.Top;
					canvas.DrawRect(frame.Left + 3, middle - 1, frame.Right - 2, middle + 2, paint);
				}

				Rect previewFrame = frame; // CameraManager.get().getFramingRectInPreview();
				float scaleX = frame.Width() / (float) previewFrame.Width();
				float scaleY = frame.Height() / (float) previewFrame.Height();

				List<ResultPoint> currentPossible = possibleResultPoints;
				List<ResultPoint> currentLast = lastPossibleResultPoints;
				if (currentPossible.Count<1) {
					lastPossibleResultPoints = null;
				} else {
					possibleResultPoints = new List<ResultPoint>(5);
					lastPossibleResultPoints = currentPossible;
					paint.Alpha=CURRENT_POINT_OPACITY;
					paint.Color= resultPointColor;
						foreach (ResultPoint point in currentPossible) {
							canvas.DrawCircle(frame.Left
								+ (int) (point.X * scaleX), frame.Top
								+ (int) (point.Y * scaleY), 6.0f, paint);
						}
				}
				if (currentLast != null) {
					paint.Alpha=CURRENT_POINT_OPACITY / 2;
					paint.Color=resultPointColor;
						foreach (ResultPoint point in currentLast) {
							canvas.DrawCircle(frame.Left
								+ (int) (point.X * scaleX), frame.Top
								+ (int) (point.Y * scaleY), 3.0f, paint);
						}
				}

				// Request another update at the animation interval, but only
				// repaint the laser line,
				// not the entire viewfinder mask.
				PostInvalidateDelayed(ANIMATION_DELAY, frame.Left, frame.Top, frame.Right, frame.Bottom);
			}
		}

		Rect GetFramingRect(Canvas canvas)
		{
			IWindowManager manager = (IWindowManager) Context.GetSystemService(Context.WindowService);
			Display display = manager.DefaultDisplay;

			Rect screenResolution = new Rect();
			display.GetRectSize (screenResolution);

			int screenx = screenResolution.Width();
			int screeny = screenResolution.Height();
			int width, height, left, top;
			if (screenx > screeny) {
				width = screenx * 16 / 100;
				height = screeny * 25 / 100;
				left = screenx * 84 / 100;
				top = screeny * 75 / 100;
			} else {
				left = screenx * 16 / 100;
				top = screeny * 25 / 100;
				width = screenx * 84 / 100;
				height = screeny * 75 / 100;
			}
			var framingRect = new Rect(left,top, width, height);

			return framingRect;
		}

		public void DrawResultBitmap(Bitmap barcode)
		{
			resultBitmap = barcode;
			Invalidate();
		}

		public void AddPossibleResultPoint(ResultPoint point)
		{
			var points = possibleResultPoints;

			lock (points)
			{
				points.Add(point);
				var size = points.Count;
				if (size > MAX_RESULT_POINTS) 
				{
					points.RemoveRange(0, size - MAX_RESULT_POINTS / 2);
				}
			}
		}
	}

}

