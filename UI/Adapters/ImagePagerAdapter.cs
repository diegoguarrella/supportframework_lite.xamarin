/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using Android.Content;
using Android.Database;
using Android.Support.V4.View;
using Android.Views;
using Java.Lang;
using QuasarTech.Xamarin.AndroidSupport.Commons;
using QuasarTech.Xamarin.AndroidSupport.UI.Widgets;
using Square.Picasso;

namespace QuasarTech.Xamarin.AndroidSupport.UI.Adapters
{
    public class ImagePagerAdapter : PagerAdapter
    {
        private List<string> imageURLs;

		public ImagePagerAdapter(Context context, List<string> imageURLs)
		{
            this.imageURLs = imageURLs;
		}
		public override Object InstantiateItem(ViewGroup container, int position)
        {
		    View view = View.Inflate(container.Context,Resource.Layout.ctrl_fullscreenimage, null);
            var ivImage = (TouchImageView)view.FindViewById(Resource.Id.ivImage);

			Picasso.With(container.Context).Load(imageURLs[position]).Into(ivImage);

            container.AddView(view);

            return view;
        }

		public override void DestroyItem(ViewGroup container, int arg1, Object arg2)
		{
			container.RemoveView((View)arg2);
		}

		public override bool IsViewFromObject(View view, Object o)
		{
			return view == o;
		}

		public override void UnregisterDataSetObserver(DataSetObserver observer)
		{
			if (observer != null)
				base.UnregisterDataSetObserver(observer);
		}

		public override int Count
		{
			get { return imageURLs.Count; }
		}
    }
}