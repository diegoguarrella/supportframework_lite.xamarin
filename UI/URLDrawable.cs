/*
 * SupportFramework_LITE
 *  - Native version: Copyright (C) 2012-2016 Diego Guarrella
 *  - Xamarin porting, integration and extension: Copyright (C) 2014-2016 Diego Guarrella
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using Android.Graphics;
using Android.Graphics.Drawables;

namespace QuasarTech.Xamarin.AndroidSupport.UI
{
    public class URLDrawable : BitmapDrawable
    {
        // the drawable that you need to set, you could set the initial drawing
        // with the loading image if you need to
        public Drawable Drawable;

        public override void Draw(Canvas canvas)
        {
			if (Drawable != null)
            {
				Drawable.Draw(canvas);
            }
        }
    }
}